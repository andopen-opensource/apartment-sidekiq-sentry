require 'apartment/sidekiq/sentry/railtie'
require 'apartment/sidekiq/sentry/middleware/server'

module Apartment
  module Sidekiq
    module Sentry
      module Middleware
        def self.run
          ::Sidekiq.configure_server do |config|
            config.server_middleware do |chain|
              chain.add ::Apartment::Sidekiq::Sentry::Middleware::Server
            end
          end
        end
      end
    end
  end
end

module Apartment::Sidekiq::Sentry::Middleware
  class Server
    def call(_worker, _job, _queue)
      return yield unless ::Sentry.initialized?

      scope = ::Sentry.get_current_scope
      scope.set_tags(tenant: Apartment::Tenant.current, domain: ::TenantSettings.site.domain)

      yield
    end
  end
end

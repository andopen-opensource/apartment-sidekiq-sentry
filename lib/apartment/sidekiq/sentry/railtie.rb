module Apartment
  module Sidekiq
    module Sentry
      class Railtie < ::Rails::Railtie
        initializer 'apartment.sidekiq.sentry', after: 'apartment.sidekiq' do
          Apartment::Sidekiq::Sentry::Middleware.run
        end
      end
    end
  end
end

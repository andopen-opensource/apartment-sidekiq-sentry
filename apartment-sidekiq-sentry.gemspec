$LOAD_PATH.push File.expand_path('lib', __dir__)

# Maintain your gem's version:
require 'apartment/sidekiq/sentry/version'

# Describe your gem and declare its dependencies:
Gem::Specification.new do |spec|
  spec.name = 'apartment-sidekiq-sentry'
  spec.version = Apartment::Sidekiq::Sentry::VERSION
  spec.authors = ['Viktor (Ikon) VAD']
  spec.email = ['vad.viktor+gitlab@gmail.com']
  spec.homepage = 'https://gitlab.com/andopen-opensource/apartment-sidekiq-sentry'
  spec.summary = 'Log tenant data for Sentry'
  spec.description = <<~TXT
    A Rails engine adding a middleware after Apartment-sidekiq to be able to log tenant data in Sentry after the tenant is set.
  TXT
  spec.license = 'MIT'

  spec.files = Dir['{app,config,db,lib}/**/*', 'LICENSE.txt', 'README.md']

  spec.add_dependency 'railties'
  spec.add_dependency 'ros-apartment-sidekiq'
end
